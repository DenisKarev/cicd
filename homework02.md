### Homework #02
## Процессы CI/CD
### Урок 2. Continuous integration (непрерывная интеграция)


### Домашнее задание:
#### 1. Переписать test stage для тестирования docker-а. Достаточно проверить, что docker контейнер на базе нашего собранного образа в предыдущей job запускается.


### 1.
https://gitlab.com/DenisKarev/cicd/-/blob/main/.gitlab-ci.yml

![hw02-docker-test-log-image](./img/hw02-docker-test-log.png)
